package com.company.lesson20v2;


import dao.EmployeeService;
import entity.Employee;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/employee")
public class HelloResource {

private EmployeeService service = new EmployeeService();

    @GET
    @Produces("application/json")
    public List<Employee> getAllEmployees() {
        List<Employee> list = service.findAll();
        return list;
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Employee getIdEmployees(@PathParam("id") int id) {
        return service.findById(id);
    }

    @POST
    @Produces("application/json")
    public void add(@QueryParam("name") String name,
                        @QueryParam("dep") String dep,
                        @QueryParam("age") int age){
        Employee employee = new Employee(name,dep,age);
        service.persist(employee);
    }

    @DELETE
    @Produces("application/json")
    public void deleteIDEmployees(@QueryParam("id") int id) {
      service.delete(id);
    }

    @PUT
    @Produces("application/json")
    public void updateIdEmployee(@QueryParam("id") int id,
                                           @QueryParam("name") String name,
                                           @QueryParam("dep") String dep,
                                           @QueryParam("age") int age
    ){
       Employee employee = new Employee();
       employee.setId(id);
       employee.setName(name);
       employee.setDep(dep);
       employee.setAge(age);

       service.update(employee);
    }
}