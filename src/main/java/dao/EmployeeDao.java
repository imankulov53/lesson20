package dao;

import entity.Employee;

import java.util.List;

public class EmployeeDao extends BaseDao<Employee,Integer> {
    @Override
    public void persist(Employee entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Employee entity) {
        getCurrentSession().merge(entity);
    }

    @Override
    public Employee findById(Integer integer) {
        Employee employee = getCurrentSession().get(Employee.class, integer);
        return employee;
    }

    @Override
    public void delete(Employee entity) {
        getCurrentSession().remove(entity);

    }

    @Override
    public List<Employee> findAll() {
        return getCurrentSession().createQuery("FROM Employee").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(client -> delete(client));
    }
}
