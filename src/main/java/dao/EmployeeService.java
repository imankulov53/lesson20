package dao;

import entity.Employee;

import java.util.List;

public class EmployeeService {

    private Dao<Employee,Integer> employeeDao;

    public EmployeeService() {
        employeeDao = new EmployeeDao();
    }

    public void persist(Employee entity){
        employeeDao.openSession();
        employeeDao.persist(entity);
        employeeDao.closeSession();
    }

    public void update(Employee entity){
        employeeDao.openSession();
        employeeDao.update(entity);
        employeeDao.closeSession();
    }

    public Employee findById(Integer integer){
        employeeDao.openSession();
        Employee client = employeeDao.findById(integer);
        employeeDao.closeSession();
        return client;
    }

    public void delete(Integer integer){
        employeeDao.openSession();
        employeeDao.delete(employeeDao.findById(integer));
        employeeDao.closeSession();
    }

    public List<Employee> findAll(){
        employeeDao.openSession();
        List<Employee> list = employeeDao.findAll();
        employeeDao.closeSession();
        return list;
    }

    public void deleteAll(){
        employeeDao.openSession();
        employeeDao.deleteAll();
        employeeDao.closeSession();
    }

}
