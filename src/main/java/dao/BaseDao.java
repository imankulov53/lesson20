package dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import util.Util;

public abstract class BaseDao<T,ID> implements Dao<T,ID> {

    private Session currentSession;

    private Transaction currentTransaction;

    public Session openSession() {
        currentSession = Util.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }


}
